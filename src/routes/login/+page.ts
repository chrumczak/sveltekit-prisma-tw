import {goto} from "$app/navigation";
import { redirect } from "@sveltejs/kit";
import type { PageLoad } from "./$types";

export const load = (async ({parent, ...test}) => {
    const {session} = await parent();

    if (session) {
        throw redirect(302, '/auth');
    }

    return {

    };
}) satisfies PageLoad;
