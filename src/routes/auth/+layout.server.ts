// src/routes/api/protected-route/+server.ts
import type { LayoutServerLoad } from './$types'
import {json, error, } from '@sveltejs/kit'

export const load: LayoutServerLoad = async ({ locals: { supabase, getSession } }) => {
    const session = await getSession()
    if (!session) {
        throw error(403, { message: 'Unauthorized' })
    }

    const { data } = await supabase.from('test').select('*')
    return {
        data
    }
}
