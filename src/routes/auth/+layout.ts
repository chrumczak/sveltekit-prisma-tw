import type { LayoutLoad } from './$types'

export const load: LayoutLoad = async ({ parent }) => {
    const parentData = await parent();
    const user = parentData?.session?.user;

    return { user }
}
